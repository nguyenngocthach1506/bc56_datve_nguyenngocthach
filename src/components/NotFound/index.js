import React, { useEffect } from 'react';
import { useNavigate } from 'react-router-dom';
import "./style.css";

export default function NotFound() {
    const navigate = useNavigate();

    useEffect(() => {
        setTimeout(() => navigate("/"), 3000)
    }, [])


    return (
        < div className='notfound' >
            <img className='notfound-img' src='./asset/img/404-error-not-found-page-lost.png' alt='NotFound'></img>
        </div >
    )
}
