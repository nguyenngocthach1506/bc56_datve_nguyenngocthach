import { combineReducers } from "redux";

/* Reducer */
import ListSeatReducer from "../../features/ListSeat/reducer/index";
import ConfirmReducer from "../../features/ListSeat/components/Confirm/reducer";
import FormRegisterReducer from "../../features/ListSeat/components/FormRegister/reducer";

const rootReducer = combineReducers({
    ListSeatReducer: ListSeatReducer,
    ConfirmReducer: ConfirmReducer,
    FormRegisterReducer: FormRegisterReducer,
});

export default rootReducer;