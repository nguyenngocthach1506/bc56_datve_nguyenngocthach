import { Routes, Route } from "react-router-dom"
/* */
import DefaultLayout from "../../layouts/DefaultLayout"
import HomePage from "../../pages/Home/index";
import TicketPage from "../../pages/Ticket/index";
import MoviesPage from "../../pages/Movies/index";
import NotFound from "../../components/NotFound";


export default function Router() {
    return (
        <Routes>
            {/* Home */}
            <Route path="/" >
                <Route index element={<DefaultLayout><HomePage /></DefaultLayout>}></Route>
                <Route path="home" element={<DefaultLayout><HomePage /></DefaultLayout>}></Route>
            </Route>
            {/* Ticket */}
            <Route path="/ticket" >
                <Route index element={<DefaultLayout><TicketPage /></DefaultLayout>}></Route>
            </Route>
            {/* Movies */}
            <Route path="/movies" >
                <Route index element={<DefaultLayout><MoviesPage /></DefaultLayout>}></Route>
            </Route>


            {/* Not Found */}
            <Route path="*" element={<NotFound></NotFound>}></Route>
        </Routes>
    )
}

