import React from 'react';
import ListSeat from "../../features/ListSeat/index";
import FormRegister from '../../features/ListSeat/components/FormRegister/index';
import ConFirm from "../../features/ListSeat/components/Confirm/index";

export default function index() {
    return (
        <>
            <h3 className='text-start w-100'>MOVIE SEAT SELECTION</h3>

            <div className="row">
                <div className='col-12 col-md-6 col-lg-8 mb-sm-3'>
                    <ListSeat></ListSeat>
                </div>
                <div className='col-12 col-md-6 col-lg-4'>
                    <FormRegister></FormRegister>
                    <ConFirm></ConFirm>
                </div>
            </div>
        </>
    )
}
