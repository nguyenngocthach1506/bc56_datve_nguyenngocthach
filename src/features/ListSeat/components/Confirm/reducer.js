import TYPE from "../../../../base/contstant/index";

const initialState = {
    name: "",
    numberSeats: "",
    seats: []
}

const ConfirmReducer = (state = initialState, { type, payload }) => {
    switch (type) {
        case TYPE.Post_Confirm: return { ...state, name: payload.name, numberSeats: payload.numberSeats, seats: payload.seats };

        default:
            return state
    }
}


export default ConfirmReducer;