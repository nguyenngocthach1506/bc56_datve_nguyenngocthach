import React, { memo } from 'react';
import { useDispatch, useSelector } from 'react-redux';
/* */
import "./style.css";
import TYPE from '../../../../base/contstant';


function Seat({ ghe, count, numberSeats }) {
    /*  */
    const dispatch = useDispatch();
    /*  */
    const { seats } = useSelector(state => state.ListSeatReducer);
    const { isValid } = useSelector(state => state.FormRegisterReducer);

    /* */
    const handeChooseSeat = (e) => {
        // Ghế đã đặt => null
        if (ghe.daDat)
            return null;

        if (isValid) {
            const newSeats = [...seats];
            let index = newSeats.findIndex(item => item.soGhe == ghe.soGhe);

            // Không tìm thấy trong danh sách = index =-1
            // Thêm vào Hook
            if (index === -1 && count.current < numberSeats) {
                newSeats.push({ ...ghe, daDat: true });
                e.target.classList.add("choosed");
                count.current++;
            }

            // Tìm thấy trong danh sách = index != -1
            // Xóa khỏi hook
            if (index !== -1 && count.current > 0) {
                newSeats.splice(index, 1);
                e.target.classList.remove("choosed");
                count.current--;
            }

            dispatch({
                type: TYPE.Post_Seats,
                payload: newSeats,
            });
        }
    }

    return (
        <a onClick={(e) => handeChooseSeat(e)} className="seat-button" role='button'>{ghe.soGhe}</a>
    )

}
export default memo(Seat);