import TYPE from "../../../base/contstant"

const initialState = {
    seats: [],
}

const ListSeatReducer = (state = initialState, { type, payload }) => {
    switch (type) {
        /* */
        case TYPE.Post_Seats: return { ...state, seats: payload };

        default:
            return state
    }
}

export default ListSeatReducer;
