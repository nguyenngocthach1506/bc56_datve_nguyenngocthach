const POST_REGISTER = "POST_REGISTER";
const CHANGE_ISVALID = "CHANGE_ISVALID";
const POST_CONFIRM = "POST_CONFIRM";
const POST_SEATS = "POST_SEATS";


const TYPE = {
    Change_isValid: CHANGE_ISVALID,
    Post_Register: POST_REGISTER,
    Post_Seats: POST_SEATS,
    Post_Confirm: POST_CONFIRM,
}
export default TYPE;