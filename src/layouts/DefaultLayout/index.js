import React from 'react';
import Header from "../../components/Header/index";
import "./style.css";

export default function DefaultLayout(props) {
    return (
        <>
            <Header></Header>
            <div className='container'>{props.children}</div>
        </>
    )
}
